package pl.kn_pirm.pwr.lembullet.events;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.model.Code;

/**
 * Created by naraj on 05.07.16.
 */
public class CodesUpdateEvent {

    public ArrayList<Code> codes;

    public CodesUpdateEvent(ArrayList<Code> codes) {
        this.codes = codes;

    }
}
