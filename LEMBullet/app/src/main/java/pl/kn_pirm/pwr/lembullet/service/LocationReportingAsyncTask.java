package pl.kn_pirm.pwr.lembullet.service;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Date;

import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.communication_util.LocationHelper;
import pl.kn_pirm.pwr.lembullet.model.LocationEntity;
import pl.kn_pirm.pwr.lembullet.model.LocationReportEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;
import pl.kn_pirm.pwr.lembullet.view.SettingsActvity;

import static java.lang.Thread.sleep;

/**
 * Created by slyboots on 27.06.16.
 */
public class LocationReportingAsyncTask extends AsyncTask<String,String,String> {

    private SettingsActvity actvity;
    public LocationReportingAsyncTask(SettingsActvity actvity){
            this.actvity=actvity;
    }
    @Override
    protected String doInBackground(String... strings) {

        int debug=0;
        while(SharedPreferences.getInstance().isReportingLocation()){
            Log.d("LRAT","loop reporting");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Location location= SharedPreferences.getInstance().getCurrentLocation();
            if(location!=null){
                Log.d("LRAT","wysylam dane przez posta "+location.getLatitude()+" "+location.getLongitude());
                LocationReportEntity entity= new LocationReportEntity();
                LocationEntity locationEntity= new LocationEntity();
                locationEntity.setLatitude(location.getLatitude()+debug);
                locationEntity.setLongitude(location.getLongitude());
                locationEntity.setTimestamp(new Date(location.getTime()));
                entity.setLocation(locationEntity);
                Log.d("LRAT","SendLocationReport");
                actvity.reportLocation(entity);
            }
            debug+=1;
        }


       // HTTPClientHelper httpClientHelper= new HTTPClientHelper();
      //  httpClientHelper.testGet();
        return null;
    }

}
