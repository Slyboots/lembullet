package pl.kn_pirm.pwr.lembullet.communication_util;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import pl.kn_pirm.pwr.lembullet.model.LocationEntity;
import pl.kn_pirm.pwr.lembullet.model.MovementEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;

/**
 * Created by slyboots on 27.06.16.
 */
public class LocationHelper   implements LocationListener {


    private static final long MIN_TIMES_BW_UPDATES = 1000;
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private static final String TAG = "LocationHelper";
    private Context context;
    private LocationManager locationManager;
    private String mprovider;
    private Location currentLocation;
    private Location prevLocation;
    private long prevTimeStamp;
    private long currTimeStamp;
    private double speed;
    public LocationHelper(Context context) {
        this.context = context;
        init();
    }

    public void init() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = locationManager.getLastKnownLocation(mprovider);
            this.currentLocation=location;
            this.prevLocation=location;
            if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIMES_BW_UPDATES ,MIN_DISTANCE_CHANGE_FOR_UPDATES , this);
            }
            else if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIMES_BW_UPDATES ,MIN_DISTANCE_CHANGE_FOR_UPDATES , this);
            }
            else {
                List<String> list = locationManager.getAllProviders();
                for (int i=0;i<list.size();i++)
                {
                    Log.d(TAG,"provider "+list.get(i));
                }
            }

            if (location != null) {
                Log.d(TAG, location.getLatitude() + " " + location.getLongitude());
                SharedPreferences.getInstance().setTripStart(location.getTime());
            } else {
                Log.d(TAG, "blad z pobieraniem lokalizacji");

            }

        }
    }

    public void stopLocationService() {
        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationManager.removeUpdates(this);
    }
    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "location changed");
        this.prevLocation=currentLocation;
        this.currentLocation=location;
        this.prevTimeStamp=this.currTimeStamp;
        this.currTimeStamp=currentLocation.getTime();
        SharedPreferences instance = SharedPreferences.getInstance();
        instance.setCurrentLocation(currentLocation);
        instance.setPrevLocation(prevLocation);
        if (prevLocation != null)
            instance.setLastDistance(prevLocation.distanceTo(currentLocation));
        instance.addDistance(instance.getLastDistance());
        instance.setPrevTimeStamp(this.prevTimeStamp);
        instance.setCurrentTimeStamp(this.currTimeStamp);
        double speed=computeSpeed
                (instance.getLastDistance(),this.prevTimeStamp,this.currTimeStamp);
        Log.d(TAG,"Last distance "+instance.getLastDistance());
        Log.d(TAG,"Time "+currentLocation.getTime());
        Log.d(TAG,"Speed "+speed);
        instance.setSpeed(speed);

        Intent intent = new Intent();
        MovementEntity entity = new MovementEntity();
        entity.setLatitude(location.getLatitude());
        entity.setLongitude(location.getLongitude());
        entity.setSpeed(speed);
        entity.setTimeStamp(location.getTime());
        intent.setAction("GPSDataUpdated");
        intent.putExtra("MovementData",entity);
        context.sendBroadcast(intent);


    }
    private double computeSpeed(double distance,Long timeStampPrev,Long timeStampCurr){


        long elapsedTime=timeStampCurr-timeStampPrev;
        Double elapsedTimeDouble= new Double(elapsedTime);
        Log.d(TAG,"elapsedTimeDouble "+elapsedTimeDouble);
        elapsedTimeDouble=elapsedTimeDouble/1000;
        Log.d(TAG,"elapsedTime s"+elapsedTimeDouble);
        return 3.6*distance/elapsedTimeDouble;
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

        Log.d(TAG, "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.d(TAG, "onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.d(TAG, "onProviderDisabled");
    }



}
