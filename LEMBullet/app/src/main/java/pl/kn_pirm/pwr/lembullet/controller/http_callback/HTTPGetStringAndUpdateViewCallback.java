package pl.kn_pirm.pwr.lembullet.controller.http_callback;

import android.util.Log;
import android.widget.Toast;

import com.studioidan.httpagent.StringCallback;

import pl.kn_pirm.pwr.lembullet.model.Code;
import pl.kn_pirm.pwr.lembullet.view.CodeContentActivity;

/**
 * Created by slyboots on 30.06.16.
 */
public class HTTPGetStringAndUpdateViewCallback extends StringCallback {

    private static final String TAG ="HTTPGETREQUESTSTRING" ;
    private CodeContentActivity activity;
    private String result;
    public HTTPGetStringAndUpdateViewCallback(CodeContentActivity activity){
        this.activity=activity;
    }
    @Override
    protected void onDone(boolean success, String stringResults) {
        Log.d(TAG,"http got respond");
        if(success){
            Log.d(TAG,"http get request "+" success ");
            activity.getCodeContentTextView().setText(stringResults);

        }
        else{
            Log.d(TAG,"http get request "+" failed ");
            Toast toast = Toast.makeText(activity, "Failed to load codes", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public CodeContentActivity getActivity() {
        return activity;
    }

    public void setActivity(CodeContentActivity activity) {
        this.activity = activity;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
