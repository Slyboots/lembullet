package pl.kn_pirm.pwr.lembullet.controller.action_listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import pl.kn_pirm.pwr.lembullet.view.CodesActivity;
import pl.kn_pirm.pwr.lembullet.view.SettingsActvity;
import pl.kn_pirm.pwr.lembullet.view.StopwatchActivity;
import pl.kn_pirm.pwr.lembullet.view.WantedActivity;

/**
 * Created by naraj on 18.06.16.
 */
public class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {

    Context context;

    public DrawerItemClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        selectItem(position);
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {

        switch (position) {
            case 0: {

                Intent intent = new Intent(context, WantedActivity.class);
                context.startActivity(intent);
                break;
            }
            case 1: {
            
                Intent intent = new Intent(context, CodesActivity.class);
                context.startActivity(intent);
                break;
            }
            case 2:{
                Intent intent = new Intent(context, StopwatchActivity.class);
                context.startActivity(intent);
                break;
            }
            case 3:{
                Intent intent = new Intent(context, SettingsActvity.class);
                context.startActivity(intent);
                break;
            }
            default:{
                break;
            }
        }
    }
}

