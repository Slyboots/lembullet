package pl.kn_pirm.pwr.lembullet.controller.http_callback;

import android.util.Log;

import com.studioidan.httpagent.JsonArrayCallback;
import com.studioidan.httpagent.JsonCallback;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by slyboots on 07.07.16.
 */
public class HttpPostLocationReportGetJsonCallback extends JsonCallback {


    private static final String TAG =HttpPostLocationReportGetJsonCallback.class.getSimpleName() ;

    @Override
    protected void onDone(boolean success, JSONObject jsonResults) {

        Log.d("HPLRGC","getRespond");
        if(success){
            Log.d(TAG,jsonResults+"");
        }
        else {
            Log.d(TAG,"failed reporting");
        }
    }
}
