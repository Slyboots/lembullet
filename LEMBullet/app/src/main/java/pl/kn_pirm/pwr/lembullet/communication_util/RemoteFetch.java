package pl.kn_pirm.pwr.lembullet.communication_util;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by naraj on 18.06.16.
 */
public class RemoteFetch {
    public static final String API_URL = "https://pacific-cove-48397.herokuapp.com/api/v1/";
    public static final String CODES_API_URL = API_URL + "codes/";

    public static JSONArray getCodesJSON(Context context) {
        try {
            URL url = new URL(CODES_API_URL);
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuilder json = new StringBuilder(1024);
            String tmp;
            while ((tmp = reader.readLine()) != null)
                json.append(tmp).append("\n");
            reader.close();

            return new JSONArray(json.toString());
        } catch (IOException e) {
            Log.e("RemoteFetch", e.getMessage());
            return null;
        } catch (Exception e) {
            Log.e("RemoteFetch", e.getMessage());
            return null;
        }
    }
}
