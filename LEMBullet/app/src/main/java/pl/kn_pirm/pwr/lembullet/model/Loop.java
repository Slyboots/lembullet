package pl.kn_pirm.pwr.lembullet.model;

/**
 * Created by naraj on 05.07.16.
 */
public class Loop {
    public long loopTime, timeFromStart;

    public Loop(long loopTime, long timeFromStart) {
        this.loopTime = loopTime;
        this.timeFromStart = timeFromStart;
    }
}
