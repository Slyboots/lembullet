package pl.kn_pirm.pwr.lembullet.controller.http_callback;

import android.util.Log;
import android.widget.Toast;

import com.studioidan.httpagent.JsonCallback;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import pl.kn_pirm.pwr.lembullet.events.TriggerCallingsUpdateEvent;
import pl.kn_pirm.pwr.lembullet.view.CallingFragment;

/**
 * Created by naraj on 30.06.16.
 */
public class CallingAcceptHTTPPostJsonCallback extends JsonCallback {

    private static final String TAG = CallingAcceptHTTPPostJsonCallback.class.getSimpleName();
    private JSONObject result = null;
    private CallingFragment fragment;

    public CallingAcceptHTTPPostJsonCallback(CallingFragment fragment) {
        this.fragment = fragment;
    }

    public JSONObject getResult() {
        return result;
    }

    public void setResult(JSONObject result) {
        this.result = result;
    }

    @Override
    protected void onDone(boolean success, JSONObject jsonResults) {

        Log.d(TAG, "http got respond");

        Log.d(TAG, getErrorMessage());
        Log.d(TAG, "Response code: " + String.valueOf(getResponseCode()));

        if (success) {
            Log.d(TAG, "http get request " + " success ");
            Log.d(TAG, jsonResults.toString());
            EventBus.getDefault().post(new TriggerCallingsUpdateEvent());
        } else {
            Log.d(TAG, "http get request " + " failed ");
            Toast.makeText(fragment.getActivity(), "Failed to send action", Toast.LENGTH_SHORT).show();
        }
    }
}
