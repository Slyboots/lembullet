package pl.kn_pirm.pwr.lembullet.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.controller.action_listener.CallingActionOnClickListener;
import pl.kn_pirm.pwr.lembullet.model.Calling;
import pl.kn_pirm.pwr.lembullet.view.CallingFragment;

/**
 * Created by naraj on 02.07.16.
 */
public class CallingListAdapter extends ArrayAdapter<Calling> {

    CallingFragment fragment;

    TextView title, description, latitude, longitude, status;
    LinearLayout pendingLayout, inProgressLayout;
    Button acceptBtn, declineBtn, completeBtn, navigateBtn;
    // Create global configuration and initialize ImageLoader with this config

    public CallingListAdapter(CallingFragment fragment, Context context, ArrayList<Calling> calling) {
        super(context, 0, calling);
        this.fragment = fragment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Calling calling = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.calling_list_item, parent, false);
        }

        initializeComponents(convertView);

        // Populate the data into the template view using the data object
        setComponents(calling);
        setButtons(calling);


        acceptBtn.setOnClickListener(new CallingActionOnClickListener(fragment, calling));
        declineBtn.setOnClickListener(new CallingActionOnClickListener(fragment, calling));
        completeBtn.setOnClickListener(new CallingActionOnClickListener(fragment, calling));
        // Return the completed view to render on screen
        return convertView;
    }

    private void initializeComponents(View view) {
        title = (TextView) view.findViewById(R.id.title_label);
        description = (TextView) view.findViewById(R.id.description_label);
        latitude = (TextView) view.findViewById(R.id.latitude_value);
        longitude = (TextView) view.findViewById(R.id.longitude_value);
        status = (TextView) view.findViewById(R.id.status_label);

        pendingLayout = (LinearLayout) view.findViewById(R.id.pending_layout);
        inProgressLayout = (LinearLayout) view.findViewById(R.id.in_progress_layout);

        acceptBtn = (Button) view.findViewById(R.id.accept_button);
        acceptBtn.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.accept_button_color), PorterDuff.Mode.MULTIPLY);
        declineBtn = (Button) view.findViewById(R.id.decline_button);
        declineBtn.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.decline_button_color), PorterDuff.Mode.MULTIPLY);
        completeBtn = (Button) view.findViewById(R.id.complete_button);
        completeBtn.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.complete_button_color), PorterDuff.Mode.MULTIPLY);
        navigateBtn = (Button) view.findViewById(R.id.navigate_button);
        navigateBtn.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.mapbox_blue), PorterDuff.Mode.MULTIPLY);
    }

    private void setComponents(Calling calling) {
        title.setText(calling.getTitle());
        description.setText(calling.getDescription());
        latitude.setText(calling.getLatitudeToString());
        longitude.setText(calling.getLongitudeToString());
        status.setText(calling.getStatusToString());
    }


    private void setButtons(Calling calling) {
        if (calling.getStatus() == Calling.CallingStatus.PENDING) {
            pendingLayout.setVisibility(View.VISIBLE);
            inProgressLayout.setVisibility(View.INVISIBLE);
        } else if (calling.getStatus() == Calling.CallingStatus.ACCEPTED) {
            pendingLayout.setVisibility(View.INVISIBLE);
            inProgressLayout.setVisibility(View.VISIBLE);
        } else if (calling.getStatus() == Calling.CallingStatus.COMPLETED) {
            pendingLayout.setVisibility(View.INVISIBLE);
            inProgressLayout.setVisibility(View.INVISIBLE);
        } else if (calling.getStatus() == Calling.CallingStatus.DECLINED) {
            pendingLayout.setVisibility(View.INVISIBLE);
            inProgressLayout.setVisibility(View.INVISIBLE);
        }

        if(calling.getLatitude() == null || calling.getLongitude() == null) {
            navigateBtn.setVisibility(View.INVISIBLE);
        } else {
            navigateBtn.setVisibility(View.VISIBLE);
        }
    }
}

