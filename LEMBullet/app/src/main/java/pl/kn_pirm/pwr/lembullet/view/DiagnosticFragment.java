package pl.kn_pirm.pwr.lembullet.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.natasa.progressviews.LineProgressBar;
import com.natasa.progressviews.utils.ProgressLineOrientation;

import org.w3c.dom.Text;

import java.util.Locale;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.brodcast_receiver.DiagnosticReceiver;
import pl.kn_pirm.pwr.lembullet.model.MovementEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;

import static java.lang.Thread.sleep;
import static pl.kn_pirm.pwr.lembullet.R.id.total_distance_text_view;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DiagnosticFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DiagnosticFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DiagnosticFragment extends Fragment {

    private static final String TAG = "DiagnosticFragment";
    private  Locale locale = Locale.getDefault();
    private IntentFilter filter;
    private LineProgressBar rangeProgressBar;
    private TextView rangeTextView;

    private LineProgressBar speedProgressBar;
    private TextView speedTextView;

    private TextView elapsedTimeTextView;
    private TextView lastTripDistanceTextView;


    private void initView(View view) {
        rangeProgressBar = (LineProgressBar) view.findViewById(R.id.range_progress_bar);
        rangeProgressBar.setLineOrientation(ProgressLineOrientation.VERTICAL);
        rangeProgressBar.setProgress(60);
        rangeProgressBar.setWidthProgressBarLine(120);
        int [] colors=new int[3];
        colors[0]= Color.RED;
        colors[1]=Color.YELLOW;
        colors[2]=Color.GREEN;

      //  rangeProgressBar.setLinearGradientProgress(true,colors);

        rangeTextView= (TextView) view.findViewById(R.id.range_progress_bar_text_view);

        rangeProgressBar.setProgress(100);
        rangeTextView = (TextView) view.findViewById(R.id.range_progress_bar_text_view);

        speedProgressBar = (LineProgressBar) view.findViewById(R.id.speed_progress_bar);
        speedProgressBar.setLineOrientation(ProgressLineOrientation.HORIZONTAL);
        speedProgressBar.setProgress(50);
        speedProgressBar.setWidthProgressBarLine(80);
        //speedTextView= (TextView) view.findViewById(R.id.speed_textView);
        speedProgressBar.setProgress(0);



        lastTripDistanceTextView = (TextView) view.findViewById(R.id.total_distance_text_view);
        elapsedTimeTextView = (TextView) view.findViewById(R.id.elapsed_time_text_view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragment_diagnostic = inflater.inflate(R.layout.diagnostic_fragment, container, false);
        initView(fragment_diagnostic);
        return fragment_diagnostic;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter =
                new IntentFilter("GPSDataUpdated");
        getActivity().registerReceiver(broadcast, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcast);
    }

    private float convertSpeedToProgress(MovementEntity entity){
            float result= (float) entity.getSpeed();
            result= (float) (result*100.0f/SharedPreferences.MAX_SPEED);
        return result;
    }
    private double computeBatteryLevel(SharedPreferences instance) {
        double result = 100;
        result = result - 100.0 * instance.getTotalDistanceInKm() / SharedPreferences.RANGE;
        if (result < 0) {
            return 0;
        } else {
            return result;
        }
    }




    private final BroadcastReceiver broadcast = new BroadcastReceiver() {
        @Override

      

        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive ");
            MovementEntity entity = (MovementEntity) intent.getSerializableExtra("MovementData");
          /*  speedProgressBar.setProgress(convertSpeedToProgress(entity));
            speedProgressBar.setText("     "+entity.getSpeed()+" km/h");
            totalDistanceTextView.setText(SharedPreferences.getInstance().getTotalDistanceInKm()+" km");
            speedTextView.setText(SharedPreferences.getInstance().getSpeed()+" km/h");
            batteryLevelProgressBar.setProgress((float) computeBatteryLevel(SharedPreferences.getInstance()));*/
            rangeProgressBar.setLinearGradientProgress(true);

            rangeProgressBar.setProgress((float) computeBatteryLevel(SharedPreferences.getInstance()));

            double rangeValue = computeBatteryLevel(SharedPreferences.getInstance());
            Locale locale = Locale.getDefault();
            String rangeText = String.format(locale, "%.0f", rangeValue) + " %";
            rangeTextView.setText(rangeText);

            speedProgressBar.setProgress(convertSpeedToProgress(entity));


            String speedText = String.format(locale, "%.0f", entity.getSpeed()) + " km/h";
            Log.d(TAG,"speedText "+speedText);
           // speedTextView.setText("");
            speedProgressBar.setText(speedText);
            long elapsedTime =  entity.getTimeStamp()- SharedPreferences.getInstance().getTripStart();


            String elapsedTimeText =convertTime(elapsedTime);
            elapsedTimeTextView.setText(elapsedTimeText);

            String lastTripDistText = String.format(locale, "%.2f", SharedPreferences.getInstance().getTotalDistanceInKm()) + " km";
            lastTripDistanceTextView.setText(lastTripDistText);


        }
    };

    private String convertTime(long elapsedTime) {
        int mili = (int) (elapsedTime % 1000);
        int seconds = (int) (elapsedTime / 1000);
        int minutes = seconds / 60;
        int hours=minutes/60;
        return String.format(locale, "%02d", hours) + "h:" +
                String.format(locale, "%02d", minutes)+"min" ;



    }
}