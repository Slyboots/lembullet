package pl.kn_pirm.pwr.lembullet.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.model.Wanted;

/**
 * Created by naraj on 02.07.16.
 */
public class WantedListAdapter extends ArrayAdapter<Wanted> {

    TextView name, alias, age, sex, height, weight, hair, eyes, race;
    ImageView photo;
    // Create global configuration and initialize ImageLoader with this config

    public WantedListAdapter(Context context, ArrayList<Wanted> wanted) {
        super(context, 0, wanted);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Wanted wanted = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.wanted_list_item, parent, false);
        }

        initializeComponents(convertView);

        // Populate the data into the template view using the data object
        setComponents(wanted);
        // Return the completed view to render on screen
        return convertView;
    }

    private void initializeComponents(View view) {
        name = (TextView) view.findViewById(R.id.wanted_name);
        alias = (TextView) view.findViewById(R.id.alias_value);
        age = (TextView) view.findViewById(R.id.age_value);
        sex = (TextView) view.findViewById(R.id.sex_value);
        height = (TextView) view.findViewById(R.id.height_value);
        weight = (TextView) view.findViewById(R.id.weight_value);
        hair = (TextView) view.findViewById(R.id.hair_value);
        eyes = (TextView) view.findViewById(R.id.eyes_value);
        race = (TextView) view.findViewById(R.id.race_value);
        photo = (ImageView) view.findViewById(R.id.wanted_photo);
    }

    private void setComponents(Wanted wanted) {
        name.setText(wanted.getName() == null ? "" : wanted.getName());
        alias.setText(wanted.getAlias() == null ? "" : wanted.getAlias());
        age.setText(wanted.getAge() == null ? "" : String.valueOf(wanted.getAge()));
        sex.setText(wanted.getGender() == null ? "" : wanted.getGender());
        height.setText(wanted.getHeight() == null ? "" : String.valueOf(wanted.getHeight()));
        weight.setText(wanted.getWeight() == null ? "" : String.valueOf(wanted.getWeight()));
        hair.setText(wanted.getHair() == null ? "" : wanted.getHair());
        eyes.setText(wanted.getEyes() == null ? "" : wanted.getEyes());
        race.setText(wanted.getRace() == null ? "" : wanted.getRace());
        if (photo != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(wanted.getPhoto_thumb_url(), photo);
        }
    }
}
