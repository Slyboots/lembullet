package pl.kn_pirm.pwr.lembullet.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by naraj on 02.07.16.
 */
public class Wanted {
    private Integer id;
    private String name, alias, gender;
    private Integer age;
    private String description;
    private Float height, weight;
    private String hair, eyes, race;
    private String photo_medium_url, photo_thumb_url, photo_small_url;

    public Wanted() {

    }

    public Wanted(int id, String name, String alias, String gender, int age, String description, float height, float weight, String hair, String eyes, String race, String photo_medium_url, String photo_thumb_url, String photo_small_url) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.age = age;
        this.description = description;
        this.height = height;
        this.weight = weight;
        this.hair = hair;
        this.eyes = eyes;
        this.race = race;
        this.photo_medium_url = photo_medium_url;
        this.photo_thumb_url = photo_thumb_url;
        this.photo_small_url = photo_small_url;
    }

    public static String parseGender(JSONObject obj) throws JSONException {
        return obj.getString("abbr");
    }

    public static Wanted fromJSON(JSONObject obj) {
        Wanted wanted = null;
        try {
            wanted = new Wanted();
            wanted.setId(obj.getString("id").equals("null") ? null : Integer.valueOf(obj.getString("id")));
            wanted.setName(obj.getString("name").equals("null") ? "" : obj.getString("name"));
            wanted.setAlias(obj.getString("alias").equals("null") ? "" : obj.getString("alias"));
            wanted.setGender(Wanted.parseGender(obj.getJSONObject("gender")));
            wanted.setAge(obj.getString("age").equals("null") ? null : Integer.valueOf(obj.getString("age")));
            wanted.setDescription(obj.getString("description").equals("null") ? "" : obj.getString("description"));
            wanted.setHeight(obj.getString("height").equals("null") ? null : Float.valueOf(obj.getString("height")));
            wanted.setWeight(obj.getString("weight").equals("null") ? null : Float.valueOf(obj.getString("weight")));
            wanted.setHair(obj.getString("hair").equals("null") ? "" : obj.getString("hair"));
            wanted.setEyes(obj.getString("eyes").equals("null") ? "" : obj.getString("eyes"));
            wanted.setRace(obj.getString("race").equals("null") ? "" : obj.getString("race"));
            wanted.setPhoto_medium_url(obj.getString("photo_medium_url").equals("null") ? null : obj.getString("photo_medium_url"));
            wanted.setPhoto_thumb_url(obj.getString("photo_thumb_url").equals("null") ? null : obj.getString("photo_thumb_url"));
            wanted.setPhoto_small_url(obj.getString("photo_small_url").equals("null") ? null : obj.getString("photo_small_url"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wanted;
    }

    public static ArrayList<Wanted> fromJSON(JSONArray jsonArray) {

        ArrayList<Wanted> wantedList = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject wanted;
                wanted = jsonArray.getJSONObject(i);
                wantedList.add(Wanted.fromJSON(wanted));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return wantedList;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public String getEyes() {
        return eyes;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getPhoto_medium_url() {
        return photo_medium_url;
    }

    public void setPhoto_medium_url(String photo_medium_url) {
        this.photo_medium_url = photo_medium_url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(id).append('\n');
        sb.append(name).append('\n');
        sb.append(alias).append('\n');
        sb.append(gender).append('\n');
        sb.append(age).append('\n');
        sb.append(race).append('\n');
        sb.append(height).append('\n');
        sb.append(weight).append('\n');
        sb.append(hair).append('\n');
        sb.append(eyes).append('\n');
        sb.append(description).append('\n');
        sb.append(photo_medium_url).append('\n');

        return sb.toString();
    }

    public String getPhoto_thumb_url() {
        return photo_thumb_url;
    }

    public void setPhoto_thumb_url(String photo_thumb_url) {
        this.photo_thumb_url = photo_thumb_url;
    }

    public String getPhoto_small_url() {
        return photo_small_url;
    }

    public void setPhoto_small_url(String photo_small_url) {
        this.photo_small_url = photo_small_url;
    }
}
