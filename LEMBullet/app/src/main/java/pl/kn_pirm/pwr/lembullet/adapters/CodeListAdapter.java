package pl.kn_pirm.pwr.lembullet.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.model.Code;
import pl.kn_pirm.pwr.lembullet.view.CodesActivity;

/**
 * Created by naraj on 18.06.16.
 */
public class CodeListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<Code> codeList;

    public CodeListAdapter(CodesActivity codesActivity, List<Code> codeList) {
        activity = codesActivity;
        this.codeList = codeList;
    }

    @Override
    public int getCount() {
        return codeList.size();
    }

    @Override
    public Object getItem(int position) {
        return codeList.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.code_list_row, null);

        TextView title = (TextView) convertView.findViewById(R.id.code_name);

        // getting movie data for the row
        Code code = codeList.get(position);

        // title
        title.setText(code.getName());

        return convertView;
    }
}
