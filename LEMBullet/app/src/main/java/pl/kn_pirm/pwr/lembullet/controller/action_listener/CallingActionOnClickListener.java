package pl.kn_pirm.pwr.lembullet.controller.action_listener;

import android.telecom.Call;
import android.view.View;

import org.apache.http.HttpClientConnection;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.model.Calling;
import pl.kn_pirm.pwr.lembullet.view.CallingFragment;

/**
 * Created by naraj on 03.07.16.
 */
public class CallingActionOnClickListener implements View.OnClickListener {

    CallingFragment fragment;
    Calling calling;

    public CallingActionOnClickListener(CallingFragment fragment, Calling calling) {
        this.fragment = fragment;
        this.calling = calling;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.accept_button:
                HTTPClientHelper.acceptCalling(fragment, calling);
                break;

            case R.id.decline_button:
                HTTPClientHelper.declineCalling(fragment, calling);
                break;

            case R.id.complete_button:
                HTTPClientHelper.completeCalling(fragment, calling);
                break;

            default:
                break;
        }
    }
}
