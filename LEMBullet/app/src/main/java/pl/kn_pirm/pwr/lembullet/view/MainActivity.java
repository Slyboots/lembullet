package pl.kn_pirm.pwr.lembullet.view;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.communication_util.LocationHelper;
import pl.kn_pirm.pwr.lembullet.controller.action_listener.DrawerItemClickListener;
import pl.kn_pirm.pwr.lembullet.model.MainTabsPagerAdapter;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;

public class MainActivity extends AppCompatActivity {

    private ViewPager pager;
    private TabLayout tabLayout;
    private ImageButton homeButton;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] navTitles;
    private LocationHelper locationHelper;
    private static MainActivity instance;

    public MainActivity() {

    }

    public void initUI() {
        pager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        FragmentManager manager = getSupportFragmentManager();
        PagerAdapter adapter = new MainTabsPagerAdapter(manager, this);
        pager.setAdapter(adapter);

        tabLayout.setupWithViewPager(pager);
        homeButton = (ImageButton) findViewById(R.id.home_button);
        homeButton.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), PorterDuff.Mode.MULTIPLY);

        // pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void initNavTitles() {
        navTitles = getResources().getStringArray(R.array.navDrawerItems);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, navTitles));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener(this));
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int gravity = GravityCompat.START;
                if (mDrawerLayout.isDrawerOpen(gravity))
                    mDrawerLayout.closeDrawer(gravity);
                else
                    mDrawerLayout.openDrawer(gravity);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        setContentView(R.layout.splash_screen);


        //display the logo during 5 secondes,
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                //set the new Content of your activity
                MainActivity.this.setContentView(R.layout.activity_main);

                Log.i("MainActivity", "onCreate");
                initUI();
                initNavTitles();
                locationHelper = new LocationHelper(getApplicationContext());
                locationHelper.init();
            }
        }.start();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("MainActivity", "onDestroy");
        locationHelper.stopLocationService();
    }

}
