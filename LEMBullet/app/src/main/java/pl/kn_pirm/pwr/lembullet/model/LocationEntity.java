package pl.kn_pirm.pwr.lembullet.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by slyboots on 25.06.16.
 * Location is a hash => { :latitude - double, :longitude - double, :created_at - timestamp}
 */
public class LocationEntity implements Serializable{

    private double latitude;
    private double longitude;
    private Date timestamp;

    public LocationEntity(){

    }
    public LocationEntity(double latitude, double longtitude, Date timestamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "LocationEntity{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", timestamp=" + timestamp +
                '}';
    }



    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
