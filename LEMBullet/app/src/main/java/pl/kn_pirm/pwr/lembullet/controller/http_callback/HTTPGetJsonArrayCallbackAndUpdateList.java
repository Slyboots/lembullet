package pl.kn_pirm.pwr.lembullet.controller.http_callback;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.studioidan.httpagent.JsonArrayCallback;

import org.json.JSONArray;

import pl.kn_pirm.pwr.lembullet.model.Code;
import pl.kn_pirm.pwr.lembullet.view.CodesActivity;

/**
 * Created by slyboots on 30.06.16.
 */
public class HTTPGetJsonArrayCallbackAndUpdateList extends JsonArrayCallback {

    private static final String TAG ="HTTPGetCallback" ;
    private JSONArray result=null;
    private CodesActivity activity;


    public HTTPGetJsonArrayCallbackAndUpdateList(CodesActivity activity){
        this.activity=activity;
    }
    public JSONArray getResult() {
        return result;
    }

    public void setResult(JSONArray result) {
        this.result = result;
    }

    @Override
    protected void onDone(boolean success, JSONArray jsonArray) {
        Log.d(TAG,"http got respond");
        if(success){
            Log.d(TAG,"http get request "+" success ");
            Log.d(TAG,jsonArray.toString());
            activity.getCodeList().addAll(Code.fromJSONArray(jsonArray));
            activity.getAdapter().notifyDataSetChanged();
            activity.hidePDialog();

        }
        else{
            Log.d(TAG,"http get request "+" failed ");
            Toast toast = Toast.makeText(activity, "Failed to load codes", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
