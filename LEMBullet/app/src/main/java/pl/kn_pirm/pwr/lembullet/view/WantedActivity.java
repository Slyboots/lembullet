package pl.kn_pirm.pwr.lembullet.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.adapters.WantedListAdapter;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.events.FailedToLoadWantedEvent;
import pl.kn_pirm.pwr.lembullet.events.WantedUpdateEvent;
import pl.kn_pirm.pwr.lembullet.model.Wanted;

/**
 * Created by naraj on 02.07.16.
 */
public class WantedActivity extends AppCompatActivity {

    // Log tag
    private static final String TAG = WantedActivity.class.getSimpleName();

    // Movies json url
    private ProgressDialog pDialog;
    private ArrayList<Wanted> wantedList = new ArrayList<>();
    private ListView listView;
    private WantedListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wanted);
        setTitle(getString(R.string.wanted_activity_title));

        listView = (ListView) findViewById(R.id.listView);
        adapter = new WantedListAdapter(this, wantedList);
        listView.setAdapter(adapter);
        listView.setEmptyView(findViewById(R.id.wanted_view_empty));
//        listView.setOnItemClickListener(new CodeItemClickListener(this));

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();
        EventBus.getDefault().register(this);

        initImageLoader();
    }

    private void initImageLoader() {
        File cacheDir = StorageUtils.getCacheDirectory(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
//                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
//                .diskCacheExtraOptions(480, 800, null)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(15 * 1024 * 1024))
                .memoryCacheSize(15 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCache(new UnlimitedDiskCache(cacheDir)) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(this)) // default
                .imageDecoder(new BaseImageDecoder(true)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateWantedList();
    }

    private void updateWantedList() {
        HTTPClientHelper.getWantedAndUpdateActivity(this);
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
        hidePDialog();
    }

    public void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public WantedListAdapter getAdapter() {
        return adapter;
    }

    public ProgressDialog getpDialog() {
        return pDialog;
    }

    public List<Wanted> getWantedList() {
        return wantedList;
    }

    public ListView getListView() {
        return listView;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWantedUpdateEvent(WantedUpdateEvent event) {
        Log.d(TAG, "onWantedUpdateEvent");
        wantedList.clear();
        adapter.addAll(event.wanted);
        adapter.notifyDataSetChanged();
        hidePDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFailedToLoadWanted(FailedToLoadWantedEvent event) {
        Log.d(TAG, "onFailedToLoadWanted");
        hidePDialog();
    }
}
