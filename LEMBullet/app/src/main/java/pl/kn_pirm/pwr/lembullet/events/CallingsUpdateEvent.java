package pl.kn_pirm.pwr.lembullet.events;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.model.Calling;

/**
 * Created by naraj on 04.07.16.
 */
public class CallingsUpdateEvent
{
    public ArrayList<Calling> callings;

    public CallingsUpdateEvent(ArrayList<Calling> callings) {
        this.callings = callings;
    }
}
