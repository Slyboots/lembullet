package pl.kn_pirm.pwr.lembullet.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.communication_util.RemoteFetch;
import pl.kn_pirm.pwr.lembullet.adapters.CodeListAdapter;
import pl.kn_pirm.pwr.lembullet.controller.action_listener.*;
import pl.kn_pirm.pwr.lembullet.controller.action_listener.CodeItemClickListener;
import pl.kn_pirm.pwr.lembullet.events.CodesUpdateEvent;
import pl.kn_pirm.pwr.lembullet.events.FailedToLoadCodesEvent;
import pl.kn_pirm.pwr.lembullet.model.Code;

public class CodesActivity extends AppCompatActivity {

    // Log tag
    private static final String TAG = CodesActivity.class.getSimpleName();

    // Movies json url
    private ProgressDialog pDialog;
    private List<Code> codeList = new ArrayList<Code>();
    private ListView listView;
    private CodeListAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codes);
        setTitle(getString(R.string.codes_activity_title));

        listView = (ListView) findViewById(R.id.list);
        adapter = new CodeListAdapter(this, codeList);
        listView.setAdapter(adapter);
        listView.setEmptyView(findViewById(R.id.codes_view_empty));
        listView.setOnItemClickListener(new CodeItemClickListener(this));

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCodesList();
    }

    private void updateCodesList() {
        HTTPClientHelper.getCodesAndUpdateActivity(this);
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
        hidePDialog();
    }

    public void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
    public CodeListAdapter getAdapter() {
        return adapter;
    }

    public ProgressDialog getpDialog() {
        return pDialog;
    }

    public List<Code> getCodeList() {
        return codeList;
    }

    public ListView getListView() {
        return listView;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCodesUpdateEvent(CodesUpdateEvent event) {
        Log.d(TAG, "onCodesUpdateEvent");
        codeList.clear();
        codeList.addAll(event.codes);
        adapter.notifyDataSetChanged();
        hidePDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFailedToLoadCodes(FailedToLoadCodesEvent event) {
        Log.d(TAG, "onFailedToLoadCodes");
        hidePDialog();
    }
}
