package pl.kn_pirm.pwr.lembullet.model;

import android.location.Location;

import pl.kn_pirm.pwr.lembullet.service.LocationReportingAsyncTask;

/**
 * Created by slyboots on 25.06.16.
 */
public class SharedPreferences {

    private SharedPreferences(){
        reportingLocation=false;
        reportingLocationPeriodSec=4000;

    }

    private static final SharedPreferences instance = new SharedPreferences();
    public static final double RANGE=70;
    public static final double MAX_SPEED=60;
    private LocationReportingAsyncTask locationReporting;
    private  boolean reportingLocation=true;
    private  int reportingLocationPeriodSec=1;
    private final int reportingLocationPeriodSecMin=1;
    private final int reportingLocationPeriodSecMax=10;
    private Location currentLocation;
    private Location prevLocation;
    private boolean generateSound;
    private double lastDistance;
    private double totalDistance=0;
    private double totalDistanceInKm=0;
    private long tripStart;

    public long getTripStart() {
        return tripStart;
    }

    public void setTripStart(long tripStart) {
        this.tripStart = tripStart;
    }

    private long prevTimeStamp;
    private long currentTimeStamp;
    private double speed;

    public void addDistance(double distance){
        totalDistance+=distance;
        totalDistanceInKm+=distance/1000.0;
    }
    public boolean isGenerateSound() {
        return generateSound;
    }

    public void setGenerateSound(boolean generateSound) {
        this.generateSound = generateSound;
    }

    public LocationReportingAsyncTask getLocationReporting() {
        return locationReporting;
    }

    public void setLocationReporting(LocationReportingAsyncTask locationReporting) {
        this.locationReporting = locationReporting;
    }
    public double getTotalDistanceInKm() {
        return totalDistanceInKm;
    }

    public void setTotalDistanceInKm(double totalDistanceInKm) {
        this.totalDistanceInKm = totalDistanceInKm;
    }
    public long getPrevTimeStamp() {
        return prevTimeStamp;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setPrevTimeStamp(long prevTimeStamp) {
        this.prevTimeStamp = prevTimeStamp;
    }

    public long getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public void setCurrentTimeStamp(long currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public double getLastDistance() {
        return lastDistance;
    }

    public void setLastDistance(double lastDistance) {
        this.lastDistance = lastDistance;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Location getPrevLocation() {
        return prevLocation;
    }

    public void setPrevLocation(Location prevLocation) {
        this.prevLocation = prevLocation;
    }

    public  boolean isReportingLocation() {
        return reportingLocation;
    }

    public  void setReportingLocation(boolean reportingLocation) {
        this.reportingLocation = reportingLocation;
    }

    public  int getReportingLocationPeriodSec() {
        return reportingLocationPeriodSec;
    }

    public  void setReportingLocationPeriodSec(int reportingLocationPeriodSec) {
        this.reportingLocationPeriodSec = reportingLocationPeriodSec;
    }

    public int getReportingLocationPeriodSecMin() {
        return reportingLocationPeriodSecMin;
    }

    public int getReportingLocationPeriodSecMax() {
        return reportingLocationPeriodSecMax;
    }

    public static SharedPreferences getInstance(){
        return instance;
    }

}
