package pl.kn_pirm.pwr.lembullet.model;

import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

import pl.kn_pirm.pwr.lembullet.service.StoperTimeTask;

public class Stopwatch {

    private Timer timer;
    private TimerTask timerTask;
    private Handler handler;

    private long startTime = 0;
    private boolean running = false;
    private boolean isStarted = false;
    private long currentTime = 0;
    private long lastLoopTime = 0;
    private long currentLoopTime = 0;


    public Stopwatch(Handler handler) {
        timer = new Timer();
        this.handler = handler;
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
        this.running = true;
        this.isStarted = true;
        this.lastLoopTime = startTime;
        startTimer();
    }

    public void stop() {
        this.running = false;
        this.isStarted = false;
        stopTimer();
    }

    public void pause() {
        this.running = false;
        currentTime = System.currentTimeMillis() - startTime;
        currentLoopTime = System.currentTimeMillis() - lastLoopTime;
        stopTimer();
    }

    public void resume() {
        this.running = true;
        this.startTime = System.currentTimeMillis() - currentTime;
        this.lastLoopTime = System.currentTimeMillis() - currentLoopTime;
        startTimer();
    }

    public void clear() {
        stop();
    }

    private void startTimer() {
        timerTask = new StoperTimeTask(handler);
        timer.schedule(timerTask, 0, 1);
    }

    private void stopTimer() {
        if (timerTask != null) {
            timerTask.cancel();
            timer.purge();
            timerTask = null;
        }
    }

    public long triggerLoop() {
        long currentTime = System.currentTimeMillis();
        long lastLoopDuration = currentTime - lastLoopTime;
        lastLoopTime = currentTime;
        return lastLoopDuration;
    }

    public long getElapsedTime() {
        return (System.currentTimeMillis() - startTime);
    }

    public boolean isStarted() {
        return isStarted;
    }
}
