package pl.kn_pirm.pwr.lembullet.communication_util;

import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.studioidan.httpagent.HttpAgent;
import com.studioidan.httpagent.JsonArrayCallback;
import com.studioidan.httpagent.JsonCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import pl.kn_pirm.pwr.lembullet.controller.http_callback.CallingAcceptHTTPPostJsonCallback;
import pl.kn_pirm.pwr.lembullet.controller.http_callback.CallingHTTPGetJsonArrayCallback;
import pl.kn_pirm.pwr.lembullet.controller.http_callback.CodesHTTPGetJsonArrayCallback;
import pl.kn_pirm.pwr.lembullet.controller.http_callback.HTTPGetStringAndUpdateViewCallback;
import pl.kn_pirm.pwr.lembullet.controller.http_callback.HttpPostLocationReportGetJsonCallback;
import pl.kn_pirm.pwr.lembullet.controller.http_callback.WantedHTTPGetJsonArrayCallback;
import pl.kn_pirm.pwr.lembullet.model.Calling;
import pl.kn_pirm.pwr.lembullet.model.Code;
import pl.kn_pirm.pwr.lembullet.model.LocationEntity;
import pl.kn_pirm.pwr.lembullet.model.LocationReportEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;
import pl.kn_pirm.pwr.lembullet.view.CallingFragment;
import pl.kn_pirm.pwr.lembullet.view.CodeContentActivity;
import pl.kn_pirm.pwr.lembullet.view.CodesActivity;
import pl.kn_pirm.pwr.lembullet.view.WantedActivity;

/**
 * Created by slyboots on 26.06.16.
 */
public class HTTPClientHelper {

    private final static String SERVER_URL = "https://lembullet.herokuapp.com";
    private final static String CODE_URL_PATTERN = "/api/codes";
    private final static String LOCATION_REPORTING_URL_PATERN = "/api/checkin";
    private final static String ACCESS_TOKEN = "a9d68d3815ab84d13c9e42fe4cc6e24c";
    private final static String ACCESS_TOKEN_NAME = "access_token";
    private static final String TAG = "HTTPClientHelper";
    private static final String WANTED_URL_PATTERN = "/api/wanted";
    private static final String CALLINGS_URL_PATTERN = "/api/callings";
    private static final String ACCEPT_CALLING_ACTION = "/accept";
    private static final String DECLINE_CALLING_ACTION = "/decline";
    private static final String COMPLETE_CALLING_ACTION = "/complete";


    public static void getCodesAndUpdateActivity(CodesActivity activity) {
        Log.d(TAG, "getCodes started");
        CodesHTTPGetJsonArrayCallback callback = new CodesHTTPGetJsonArrayCallback(activity);
        makeGETRequestWithJSONArrayRespond
                (callback, SERVER_URL, CODE_URL_PATTERN, ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }

    public static void getWantedAndUpdateActivity(WantedActivity activity) {
        Log.d(TAG, "getWanted started");

        WantedHTTPGetJsonArrayCallback callback = new WantedHTTPGetJsonArrayCallback(activity);
        makeGETRequestWithJSONArrayRespond
                (callback, SERVER_URL, WANTED_URL_PATTERN, ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }

    public static void getCallingsAndUpdateFragment(CallingFragment fragment) {
        Log.d(TAG, "getWanted started");

        CallingHTTPGetJsonArrayCallback callback = new CallingHTTPGetJsonArrayCallback(fragment);
        makeGETRequestWithJSONArrayRespond
                (callback, SERVER_URL, CALLINGS_URL_PATTERN, ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }
    public static void reportLocation(){
        Random rand = new Random();

            Log.d("LRAT","loop reporting");
            Location location= SharedPreferences.getInstance().getCurrentLocation();
            if(location!=null){
                Log.d("LRAT","wysylam dane przez posta "+location.getLatitude()+" "+location.getLongitude());
                LocationReportEntity entity= new LocationReportEntity();
                LocationEntity locationEntity= new LocationEntity();
                locationEntity.setLatitude(location.getLatitude()+5*rand.nextDouble());
                locationEntity.setLongitude(location.getLongitude());
                locationEntity.setTimestamp(new Date(location.getTime()));
                entity.setLocation(locationEntity);
                Log.d("LRAT","SendLocationReport");
                sendLocationReport(entity);


        }


        // HTTPClientHelper httpClientHelper= new HTTPClientHelper();
        //  httpClientHelper.testGet();

    }
    public static void sendLocationReport(LocationReportEntity entity){

        entity.setAccessToken(ACCESS_TOKEN);
        Gson gson = new Gson();
        String content = gson.toJson(entity);
        Log.d(TAG," content "+content);

        HttpAgent.post(SERVER_URL +  LOCATION_REPORTING_URL_PATERN)
                .queryParams(ACCESS_TOKEN_NAME,ACCESS_TOKEN)
                .withBody(content)
                .goJson(new HttpPostLocationReportGetJsonCallback());

    }

    public static void getCodeByIdAndUpdateView(CodeContentActivity activity, Code entity) {
        makeGETRequestWithStringRespond(activity, SERVER_URL, CODE_URL_PATTERN, entity, ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }

    public static String makeGETRequestWithStringRespond(CodeContentActivity activity,
                                                         String serverURL, String apiPattern, Code entity,
                                                         String... queryParams) {

        HTTPGetStringAndUpdateViewCallback callback = new HTTPGetStringAndUpdateViewCallback(activity);
        HttpAgent.get(serverURL + apiPattern + "/" + entity.getId())
                .queryParams(queryParams)
                .goString(callback);
        return callback.getResult();
    }

    public static void makeGETRequestWithJSONArrayRespond(JsonArrayCallback callback,
                                                          String serverURL, String apiPattern, String... queryParams) {

        HttpAgent.get(serverURL + apiPattern)
                .queryParams(queryParams)
                .goJsonArray(callback);
    }

    public static void acceptCalling(CallingFragment fragment, Calling calling) {
        CallingAcceptHTTPPostJsonCallback callback = new CallingAcceptHTTPPostJsonCallback(fragment);
        respondToCalling(callback, SERVER_URL, CALLINGS_URL_PATTERN, calling, ACCEPT_CALLING_ACTION,
                        ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }

    public static void declineCalling(CallingFragment fragment, Calling calling) {
        CallingAcceptHTTPPostJsonCallback callback = new CallingAcceptHTTPPostJsonCallback(fragment);
        respondToCalling(callback, SERVER_URL, CALLINGS_URL_PATTERN, calling, DECLINE_CALLING_ACTION,
                ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }

    public static void completeCalling(CallingFragment fragment, Calling calling) {
        CallingAcceptHTTPPostJsonCallback callback = new CallingAcceptHTTPPostJsonCallback(fragment);
        respondToCalling(callback, SERVER_URL, CALLINGS_URL_PATTERN, calling, COMPLETE_CALLING_ACTION,
                ACCESS_TOKEN_NAME, ACCESS_TOKEN);
    }

    public static void respondToCalling(JsonCallback callback, String serverURL, String apiPattern, Calling entity, String action,
                                        String... queryParams) {

        HttpAgent.post(serverURL + apiPattern + "/" + entity.getId() + action)
                .queryParams(queryParams)
                .goJson(callback);
    }

    public static void testGet() {
        //HttpAgent.get("https://lembullet.herokuapp.com/api/codes?access_token=6061335b7793dba7a148962b8ffd2c86").
        HttpAgent.get("https://lembullet.herokuapp.com/api/codes?access_token=7b6bcb11919a3a8d810e7a796b08c88d")
                .goJsonArray(new JsonArrayCallback() {
                    @Override
                    protected void onDone(boolean success, JSONArray jsonArray) {
                        Log.d("http", "udalo sie " + success);
                        if (success) {
                            Log.d("http", jsonArray.toString());
                        }
                    }
                });


    }

    public void reportLocation(String url, LocationEntity entity) {
        Gson gson = new Gson();
        String content = gson.toJson(entity);
        Log.d("HTTP Helper", content);
        HttpAgent.post(url)
                //  .queryParams("key_1","value_1","key_2","value_2","key_N","value_N")
                .withBody(content)
                .goJson(new JsonCallback() {
                    @Override
                    protected void onDone(boolean success, JSONObject jsonResults) {
                        if (success) {
                            Log.d("HTTP Helper", jsonResults.toString());
                        } else {
                            Log.d("HTTP Helper", "failed");
                        }
                    }

                });
    }

    public static void makeRequest(String path, Map params) throws Exception {
        InputStream is = null;
        OutputStream os = null;
        HttpURLConnection conn = null;
        try {
            //constants
            LocationEntity entity = new LocationEntity();
            entity.setLatitude(50.51);
            entity.setLongitude(24.42);
            entity.setTimestamp(new Date());

            Gson gson = new Gson();

            // 2. Java object to JSON, and assign to a String
            String jsonInString = gson.toJson(entity);

            URL url = new URL(SERVER_URL);


            String message = new JSONObject().toString();

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(jsonInString.getBytes().length);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            // conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //open
            conn.connect();

            //setup send
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            //clean up
            os.flush();

            //do somehting with response
            is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

        } finally {

            os.close();
            is.close();
            conn.disconnect();

        }
    }
}
