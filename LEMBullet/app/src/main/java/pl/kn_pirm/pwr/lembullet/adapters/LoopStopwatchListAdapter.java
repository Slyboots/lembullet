package pl.kn_pirm.pwr.lembullet.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.model.Loop;
import pl.kn_pirm.pwr.lembullet.view.StopwatchActivity;

public class LoopStopwatchListAdapter extends ArrayAdapter<Loop> {

    private List<Loop> loopList;
    public LoopStopwatchListAdapter(Context context, int resource, List<Loop> objects) {
        super(context, resource, objects);
        this.loopList= new ArrayList<Loop>();
        this.loopList.addAll(objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.loop_row, parent, false);
        }
        TextView loopTimeTV = (TextView) convertView.findViewById(R.id.loop_time_text_view);
        TextView timeFromStartTV= (TextView) convertView.findViewById(R.id.time_from_start_text_view);

        // Get the data item for this position
        Loop loop = getItem(position);

        loopTimeTV.setText(StopwatchActivity.convertTime(loop.loopTime));
        loopTimeTV.setTextColor(Color.RED);
        timeFromStartTV.setText(StopwatchActivity.convertTime(loop.timeFromStart));


        return convertView;
    }

    @Override
    public Loop getItem(int position) {
          return  this.loopList.get(loopList.size()-1 -position);
    }

    @Override
    public void add(Loop object) {
        super.add(object);
        this.loopList.add(object);
    }
}
