package pl.kn_pirm.pwr.lembullet.controller.action_listener;

import android.provider.Settings;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;

/**
 * Created by slyboots on 26.06.16.
 */
public class SettingSeekBarChangeListener implements  SeekBar.OnSeekBarChangeListener{
    private static final String TAG =SettingSeekBarChangeListener.class.getSimpleName() ;

    private TextView choosenPeriodTextView;
    public SettingSeekBarChangeListener(TextView choosenPeriodTextView){
        this.choosenPeriodTextView=choosenPeriodTextView;
    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        SharedPreferences.getInstance().setReportingLocationPeriodSec(progress+1);

        Log.d(TAG,"progress "+progress);
        Log.d("setting seekbar","curr value "+SharedPreferences.getInstance().getReportingLocationPeriodSec());
        choosenPeriodTextView.setText(SharedPreferences.getInstance().getReportingLocationPeriodSec()+" s");

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
