package pl.kn_pirm.pwr.lembullet.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by naraj on 18.06.16.
 */
public class Code implements Serializable{

    private int id;
    private String name, text;

    public Code() {
    }

    public Code(int id, String name, String text) {
        this.id = id;
        this.name = name;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static Code fromJSON(JSONObject obj) {
        Code code = null;
        try {
            code = new Code();
            code.setId(Integer.parseInt(obj.getString("id")));
            code.setName(obj.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return code;
    }

    public static ArrayList<Code> fromJSONArray(JSONArray arr) {
        ArrayList<Code> codeList = new ArrayList<>();
        try {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject code;
                code = arr.getJSONObject(i);
                codeList.add(Code.fromJSON(code));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return codeList;
    }
}
