package pl.kn_pirm.pwr.lembullet.view;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.adapters.LoopStopwatchListAdapter;
import pl.kn_pirm.pwr.lembullet.model.Loop;
import pl.kn_pirm.pwr.lembullet.model.Stopwatch;

public class StopwatchActivity extends AppCompatActivity {
    private static final String TAG = "StopwatchActivity";

    private ArrayList<Loop> loopsList;
    private LoopStopwatchListAdapter adapter;

    private View stoppedLayout;
    private View startedLayout;

    private TextView elapsedTimeTextView;
    private ListView loopsTimeListView;

    private Button startButton, pauseButton, loopButton, clearButton;

    private Stopwatch stopwatch;
    private static Locale locale = Locale.getDefault();

    private void initView() {
        stoppedLayout = findViewById(R.id.stoppped_layout);
        startedLayout = findViewById(R.id.started_layout);

        elapsedTimeTextView = (TextView) findViewById(R.id.elapsed_time_stoper_text_view);
        loopsTimeListView = (ListView) findViewById(R.id.loops_list_view);

        startButton = (Button) findViewById(R.id.start_btn);
        pauseButton = (Button) findViewById(R.id.pause_btn);
        loopButton  = (Button) findViewById(R.id.loop_btn);
        clearButton = (Button) findViewById(R.id.clear_btn);

        startButton.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        pauseButton.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        loopButton.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        clearButton.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);

        showStartButtons();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_stoper);

        setTitle(getString(R.string.stopwatch_title));

        initView();

        loopsList = new ArrayList<>();
        adapter = new LoopStopwatchListAdapter(getApplicationContext(), 0, loopsList);
        loopsTimeListView.setAdapter(adapter);

        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Log.d(TAG, "handleMessage");
                elapsedTimeTextView.setText(convertTime(stopwatch.getElapsedTime()));
               // elapsedTimeTextView.setTextColor(Color.RED);
                return false;
            }
        });

        stopwatch = new Stopwatch(handler);
    }

    public void startStopwatch(View view) {
        if (stopwatch.isStarted())
            stopwatch.resume();
        else
            stopwatch.start();

        hideStartButtons();
    }

    public void pauseStopwatch(View view) {
        loopStopwatch(view);
        stopwatch.pause();
        showStartButtons();
        startButton.requestFocus();

    }


    public void loopStopwatch(View view) {
        long loopDuration = stopwatch.triggerLoop();
        long elapsedTime = stopwatch.getElapsedTime();
        scrollLoopListToBottom();
        adapter.add(new Loop(loopDuration, elapsedTime));
        adapter.notifyDataSetChanged();
    }

    public void clearStopwatch(View view) {
        stopwatch.clear();
        adapter.clear();
        adapter.notifyDataSetChanged();
        elapsedTimeTextView.setText(convertTime(0));
    }

    private void showStartButtons() {
        startedLayout.setVisibility(View.INVISIBLE);
        stoppedLayout.setVisibility(View.VISIBLE);
        startButton.requestFocus();

    }

    private void hideStartButtons() {
        startedLayout.setVisibility(View.VISIBLE);
        stoppedLayout.setVisibility(View.INVISIBLE);
    }

    private void scrollLoopListToBottom() {
        loopsTimeListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
               // loopsTimeListView.setSelection();
            }
        });
    }

    public static String convertTime(long elapsedTime) {
        int mili = (int) (elapsedTime % 1000);
        int seconds = (int) (elapsedTime / 1000);
        int minutes = seconds / 60;
        return String.format(locale, "%02d", minutes) + ":" +
               String.format(locale, "%02d", seconds) + ":" +
               String.format(locale, "%03d", mili);

    }
}
