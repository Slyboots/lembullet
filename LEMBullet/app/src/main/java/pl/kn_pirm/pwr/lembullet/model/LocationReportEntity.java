package pl.kn_pirm.pwr.lembullet.model;

/**
 * Created by slyboots on 06.07.16.
 */
public class LocationReportEntity {
    private LocationEntity location;
    private String accessToken;

    public LocationEntity getLocation() {
        return location;
    }

    public void setLocation(LocationEntity location) {
        this.location = location;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public LocationReportEntity(){

    }
    public LocationReportEntity(LocationEntity location, String accessToken) {
        this.location = location;
        this.accessToken = accessToken;
    }
}
