package pl.kn_pirm.pwr.lembullet.controller.http_callback;

import android.util.Log;
import android.widget.Toast;

import com.studioidan.httpagent.JsonArrayCallback;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.events.CallingsUpdateEvent;
import pl.kn_pirm.pwr.lembullet.model.Calling;
import pl.kn_pirm.pwr.lembullet.view.CallingFragment;

/**
 * Created by naraj on 30.06.16.
 */
public class CallingHTTPGetJsonArrayCallback extends JsonArrayCallback {

    private static final String TAG = CallingHTTPGetJsonArrayCallback.class.getSimpleName();
    private JSONArray result = null;
    private CallingFragment fragment;

    public CallingHTTPGetJsonArrayCallback(CallingFragment fragment) {
        this.fragment = fragment;
    }

    public JSONArray getResult() {
        return result;
    }

    public void setResult(JSONArray result) {
        this.result = result;
    }

    @Override
    protected void onDone(boolean success, JSONArray jsonArray) {
        Log.d(TAG, "http got respond");
        if (success) {
            Log.d(TAG, "http get request " + " success ");
            Log.d(TAG, jsonArray.toString());
            ArrayList<Calling> callingsList = Calling.fromJSON(jsonArray);
            EventBus.getDefault().post(new CallingsUpdateEvent(callingsList));

        } else {
            Log.d(TAG, "http get request " + " failed ");
        }

    }
}
