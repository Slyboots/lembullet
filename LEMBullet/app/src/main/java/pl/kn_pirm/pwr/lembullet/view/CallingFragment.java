package pl.kn_pirm.pwr.lembullet.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.adapters.CallingListAdapter;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.events.CallingsUpdateEvent;
import pl.kn_pirm.pwr.lembullet.events.TriggerCallingsUpdateEvent;
import pl.kn_pirm.pwr.lembullet.model.Calling;

public class CallingFragment extends Fragment {

    // Log tag
    private static final String TAG = WantedActivity.class.getSimpleName();
    private static final int UPDATE_PERIOD_SEC = 5;

    // Movies json url
    private ProgressDialog pDialog;
    private ArrayList<Calling> callingsList = new ArrayList<>();
    private ListView listView;
    private CallingListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//
//        pDialog = new ProgressDialog(getContext());
//        // Showing progress dialog before making http request
//        pDialog.setMessage("Loading...");
//        pDialog.show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                updateCallingsList();
                handler.postDelayed(this, UPDATE_PERIOD_SEC * 1000);
            }
        }, UPDATE_PERIOD_SEC * 1000);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View requestView = inflater.inflate(R.layout.fragment_request, container, false);
        listView = (ListView) requestView.findViewById(R.id.callings_list_view);
        adapter = new CallingListAdapter(this, getContext(), callingsList);
        listView.setAdapter(adapter);
        listView.setEmptyView(requestView.findViewById(R.id.callings_view_empty));
//        listView.setOnItemClickListener(new CodeItemClickListener(this));

        return requestView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCallingsList();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void updateCallingsList() {
        HTTPClientHelper.getCallingsAndUpdateFragment(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    public void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public CallingListAdapter getAdapter() {
        return adapter;
    }

    public ProgressDialog getpDialog() {
        return pDialog;
    }

    public List<Calling> getCallingsList() {
        return callingsList;
    }

    public ListView getListView() {
        return listView;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCallingsUpdateEvent(CallingsUpdateEvent event) {
        Log.d("CallingsFragment", "onCallingsUpdateEvent");
        callingsList.clear();
        adapter.addAll(event.callings);
        adapter.notifyDataSetChanged();
        hidePDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTriggerCallingsUpdateEvent(TriggerCallingsUpdateEvent event) {
        Log.d("CallingsFragment", "TriggerCallingsUpdateEvent");
        updateCallingsList();
    }
}
