package pl.kn_pirm.pwr.lembullet.controller.http_callback;

import android.util.Log;
import android.widget.Toast;

import com.studioidan.httpagent.JsonArrayCallback;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.events.FailedToLoadWantedEvent;
import pl.kn_pirm.pwr.lembullet.events.WantedUpdateEvent;
import pl.kn_pirm.pwr.lembullet.model.Wanted;
import pl.kn_pirm.pwr.lembullet.view.WantedActivity;

/**
 * Created by naraj on 30.06.16.
 */
public class WantedHTTPGetJsonArrayCallback extends JsonArrayCallback {

    private static final String TAG = WantedHTTPGetJsonArrayCallback.class.getSimpleName();
    private JSONArray result = null;
    private WantedActivity activity;

    public WantedHTTPGetJsonArrayCallback(WantedActivity activity) {
        this.activity = activity;
    }

    public JSONArray getResult() {
        return result;
    }

    public void setResult(JSONArray result) {
        this.result = result;
    }

    @Override
    protected void onDone(boolean success, JSONArray jsonArray) {
        Log.d(TAG, "http got respond");
        if (success) {
            Log.d(TAG, "http get request " + " success ");
            Log.d(TAG, jsonArray.toString());
            ArrayList<Wanted> wanted = Wanted.fromJSON(jsonArray);
            EventBus.getDefault().post(new WantedUpdateEvent(wanted));
        } else {
            Log.d(TAG, "http get request " + " failed ");
            Toast toast = Toast.makeText(activity, "Failed to load wanted list", Toast.LENGTH_SHORT);
            toast.show();
            EventBus.getDefault().post(new FailedToLoadWantedEvent());
        }

    }
}
