package pl.kn_pirm.pwr.lembullet.view;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.model.MovementEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;

public class MapFragment extends Fragment implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 1;
    private static final int RETURN_TO_MY_LOCATION_ANIM_TIME = 300;
    private static final long CHECKIN_INTERVAL_TIME = 10000;
    private static final String TAG = "MapFragment";
    private MapView mapView;
    private FloatingActionButton moveCameraToCurrentLocationBtn;
    boolean followModeOne = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View android = inflater.inflate(R.layout.fragment_map, container, false);

        moveCameraToCurrentLocationBtn = (FloatingActionButton) android.findViewById(R.id.moveCameraButton);
        moveCameraToCurrentLocationBtn.setOnClickListener(this);
        setCheckin();

        return android;
    }

    private void setCheckin() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, CHECKIN_INTERVAL_TIME);
            }
        }, CHECKIN_INTERVAL_TIME);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
        }

        if (view != null) {
            mapView = (MapView) view.findViewById(R.id.mapview);
            mapView.onCreate(savedInstanceState);

        }

        super.onViewCreated(view, savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.setMyLocationEnabled(true);
                // mapboxMap.getMaxZoom();
            }
        });
        IntentFilter filter =
                new IntentFilter("GPSDataUpdated");
        getActivity().registerReceiver(broadcast, filter);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Log.d("Main activity ","reporting start");
                HTTPClientHelper.reportLocation();
                handler.postDelayed(this, 1000);
            }
        },   1000);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().unregisterReceiver(broadcast);
    }

    /*    private void moveCameraToMyLocation() {

            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(@NonNull MapboxMap mapboxMap) {
                    Location myLocation = mapboxMap.getMyLocation();
                    if (myLocation != null) {
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(myLocation))
                                .build();

                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), RETURN_TO_MY_LOCATION_ANIM_TIME, null);
                    } else {
                        Toast.makeText(getContext(), R.string.no_location_message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }*/
    private void moveCameraToMyLocation() {

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                Location myLocation = SharedPreferences.getInstance().getCurrentLocation();
                if (myLocation != null) {
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(myLocation))
                            .build();
                    mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    //mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), RETURN_TO_MY_LOCATION_ANIM_TIME, null);

                } else {
                    Toast.makeText(getContext(), R.string.no_location_message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (followModeOne) {
            followModeOne = false;
            Toast.makeText(getContext(), R.string.follow_mode_off, Toast.LENGTH_SHORT).show();
        } else {
            moveCameraToMyLocation();
            followModeOne = true;
            Toast.makeText(getContext(), R.string.follow_mode_on, Toast.LENGTH_SHORT).show();
        }

    }

    private final BroadcastReceiver broadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive ");
            MovementEntity entity = (MovementEntity) intent.getSerializableExtra("MovementData");
            if (followModeOne) {
                moveCameraToMyLocation();
            }

        }
    };
}
