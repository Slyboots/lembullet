package pl.kn_pirm.pwr.lembullet.controller.action_listener;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import pl.kn_pirm.pwr.lembullet.model.Code;
import pl.kn_pirm.pwr.lembullet.view.CodeContentActivity;
import pl.kn_pirm.pwr.lembullet.view.CodesActivity;

/**
 * Created by slyboots on 30.06.16.
 */
public class CodeItemClickListener implements AdapterView.OnItemClickListener {

    private static final String TAG = CodeItemClickListener.class.getSimpleName();
    private CodesActivity activity;

    public CodeItemClickListener(CodesActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "clicked on item");
        Intent intent = new Intent(activity, CodeContentActivity.class);
        intent.putExtra("CodeEntity", activity.getCodeList().get(i));


        activity.startActivity(intent);
        Log.d(TAG, "activity started");
    }
}
