package pl.kn_pirm.pwr.lembullet.brodcast_receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.natasa.progressviews.LineProgressBar;

import pl.kn_pirm.pwr.lembullet.R;

public class DiagnosticReceiver extends BroadcastReceiver {
    private static final String TAG ="DiagnosticReceiver" ;
    private LineProgressBar speedBar;
    public DiagnosticReceiver() {
    }
    public DiagnosticReceiver(LineProgressBar speedBar){
        this.speedBar=speedBar;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG,"onReceive ");
        Log.d(TAG," "+speedBar);

        //speedBar.setProgress((float) pl.kn_pirm.pwr.lembullet.model.SharedPreferences.getInstance().getSpeed());

    }
}
