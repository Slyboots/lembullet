package pl.kn_pirm.pwr.lembullet.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.communication_util.LocationHelper;
import pl.kn_pirm.pwr.lembullet.controller.action_listener.SettingSeekBarChangeListener;
import pl.kn_pirm.pwr.lembullet.model.LocationReportEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;
import pl.kn_pirm.pwr.lembullet.service.LocationReportingAsyncTask;
import pl.kn_pirm.pwr.lembullet.service.SoundsGeneratorService;

public class SettingsActvity extends AppCompatActivity {

    private  SeekBar reportingSeekBar;
    private CheckBox reportingCheckBox;
    private CheckBox soundGenerationCheckBox;
    private TextView choosenPeriodTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_actvity);
        setTitle(getString(R.string.settings_activity_title));

        initSeekBar();
        initChoosenPeriodTextView();
        initCheckbox();


    }

    private void initCheckbox() {
        soundGenerationCheckBox= (CheckBox) findViewById(R.id.sound_generation_checkbox);
        soundGenerationCheckBox.setChecked(SharedPreferences.getInstance().isGenerateSound());
    }

    public void onSoundGenerationCheckBoxClicked(View view){
        if(soundGenerationCheckBox.isChecked()){
            startService(new Intent(this,SoundsGeneratorService.class));
            soundGenerationCheckBox.setChecked(true);
            SharedPreferences.getInstance().setGenerateSound(true);

        }
        else{
            soundGenerationCheckBox.setChecked(false);
            SharedPreferences.getInstance().setGenerateSound(false);
            stopService(new Intent(this,SoundsGeneratorService.class));

        }

    }
    public void onCheckboxClicked(View view){
        LocationHelper locationHelper= new LocationHelper(getApplicationContext());
        Log.d("SettingsActivity","checkBox.isChecked() "+reportingCheckBox.isChecked());
        Log.d("SettingsActivity","setReportingLocation before"+SharedPreferences.getInstance().isReportingLocation());
       //SharedPreferences.getInstance().setLocationReporting(new LocationReportingAsyncTask(this));
       // SharedPreferences.getInstance().getLocationReporting().execute("");
        if(reportingCheckBox.isChecked()){
            SharedPreferences.getInstance().setReportingLocation(true);
            Log.d("SettingsActivity","setReportingLocation "+SharedPreferences.getInstance().isReportingLocation());

        }
        else{
            SharedPreferences.getInstance().setReportingLocation(false);
            Log.d("SettingsActivity","setReportingLocation "+SharedPreferences.getInstance().isReportingLocation());
        }
    }
  /*  private void initCheckbox(){
        reportingCheckBox= (CheckBox) findViewById(R.id.location_reporting_checkbox);
        reportingCheckBox.setChecked(SharedPreferences.getInstance().isReportingLocation());
        soundGenerationCheckBox= (CheckBox) findViewById(R.id.sound_generation_checkbox);

    }*/
    private void initSeekBar(){
        reportingSeekBar = (SeekBar) findViewById(R.id.location_reporting_period_seek_bar);
        reportingSeekBar.setMax(SharedPreferences.getInstance().getReportingLocationPeriodSecMax());

    }
    private void initChoosenPeriodTextView(){
        choosenPeriodTextView= (TextView) findViewById(R.id.choosen_period_text_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        reportingSeekBar.setOnSeekBarChangeListener(new SettingSeekBarChangeListener(choosenPeriodTextView));
    }

    public void reportLocation(LocationReportEntity entity){
        HTTPClientHelper.sendLocationReport(entity);
    }
}
