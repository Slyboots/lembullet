package pl.kn_pirm.pwr.lembullet.view;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.lang.reflect.Method;

import pl.kn_pirm.pwr.lembullet.R;

public class CodeViewActivity extends AppCompatActivity {

    public static final String CODE_VIEW_TAG = "CODE_VIEW_URL";
    public static final String CODE_NAME_TAG = "CODE_NAME";
    private static final int SEARCH_MENU_ID = Menu.FIRST;
    WebView codeWebView;
    private LinearLayout container;
    private Button nextButton;
    private Button closeButton;
    private EditText findBox;
    private boolean isSearchEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_view);

        codeWebView = (WebView) findViewById(R.id.code_webview);
        codeWebView.getSettings().setBuiltInZoomControls(true);
        codeWebView.getSettings().setDisplayZoomControls(false);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String codeViewUrl = extras.getString(CODE_VIEW_TAG);
            String codeName = extras.getString(CODE_NAME_TAG);
            //The key argument here must match that used in the other activity
            setTitle(codeName);
            codeWebView.loadUrl("https://lembullet.herokuapp.com/api/codes/1?access_token=7b6bcb11919a3a8d810e7a796b08c88d");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(0, SEARCH_MENU_ID, 0, "Search");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case SEARCH_MENU_ID:
                search();
                return true;
        }
        return true;
    }

    public void search() {
        if (!isSearchEnabled) {
            container = (LinearLayout) findViewById(R.id.search_layout);

            nextButton = new Button(this);
            nextButton.setText("Next");
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    codeWebView.findNext(true);
                }
            });
            container.addView(nextButton);

            closeButton = new Button(this);
            closeButton.setText("Close");
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    container.removeAllViews();
                    isSearchEnabled = false;
                }
            });
            container.addView(closeButton);

            findBox = new EditText(this);
            findBox.setMinEms(30);
            findBox.setSingleLine(true);
            findBox.setHint("Search");

            findBox.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && ((keyCode == KeyEvent.KEYCODE_ENTER))) {
                        codeWebView.findAllAsync(findBox.getText().toString());

                        try {
                            Method m = WebView.class.getMethod("setFindIsUp", Boolean.TYPE);
                            m.invoke(codeWebView, true);
                        } catch (Exception ignored) {
                        }
                    }
                    return false;
                }
            });

            container.addView(findBox);
            isSearchEnabled = true;
        }
    }
}
