package pl.kn_pirm.pwr.lembullet.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.model.MovementEntity;
import pl.kn_pirm.pwr.lembullet.model.SharedPreferences;

public class SoundsGeneratorService extends Service implements MediaPlayer.OnCompletionListener {
    private MediaPlayer mediaPlayer;
    private double [] previousTimes= new double[10];



    @Override
    public void onCreate() {
        super.onCreate();


        mediaPlayer = MediaPlayer.create(this, R.raw.aceleration_mp3sound);// raw/s.mp3
        //mediaPlayer=MediaPlayer.create(this,R.raw.aceleration_mp3sound);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.setLooping(true);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setVolume(0.5f,0.5f);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
        IntentFilter filter =
                new IntentFilter("GPSDataUpdated");
        registerReceiver(broadcast, filter);
        return START_NOT_STICKY;
    }
    public void onDestroy() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.release();
        unregisterReceiver(broadcast);
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        stopSelf();
    }
    private float convertSpeedToVolume(double speed){
        float result= (float) (speed/SharedPreferences.getInstance().MAX_SPEED);
        result+=0.5;
        if(result>1){
            return 1;
        }
        else{
            return  result;
        }

    }
    private final BroadcastReceiver broadcast = new BroadcastReceiver()
    {
        public static final String TAG ="SoundGeneratorService";

        @Override
        public void onReceive(Context context, Intent intent)
        {

            Log.d(TAG,"onReceive ");
            MovementEntity entity= (MovementEntity) intent.getSerializableExtra("MovementData");
            float volume=convertSpeedToVolume(entity.getSpeed());
            Log.d(TAG,"volume "+volume);
            mediaPlayer.setVolume(volume,volume);
            Log.d(TAG," position "+mediaPlayer.getCurrentPosition());
       /*     int iSoundId = (Integer) mSounds.get(R.raw.motor_sound1);
            mShortPlayer.play(iSoundId, 0.99f, 0.99f, 0, 0, 1);*/

            //mediaPlayer.seekTo(convertSpeedToSeek(entity.getSpeed()));

        }

    };
    private int convertSpeedToSeek(double speed){
        int range=13;
        double percent=speed/SharedPreferences.MAX_SPEED;
        if(percent>1){
            return range;
        }
        else{
            return (int) (range*(percent));
        }

    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}