package pl.kn_pirm.pwr.lembullet.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.communication_util.HTTPClientHelper;
import pl.kn_pirm.pwr.lembullet.model.Code;

public class CodeContentActivity extends AppCompatActivity {

    private static final String TAG ="CodeContentActivity" ;
    private TextView codeContentTextView;
    private Code codeEntity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_code_content);
        codeEntity= (Code) getIntent().getSerializableExtra("CodeEntity");
        setTitle(codeEntity.getName());
        Log.d(TAG,"codeEntity "+codeEntity.getName()+" "+codeEntity.getId());
        codeContentTextView= (TextView) findViewById(R.id.code_content_text_view);
        codeContentTextView.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void onResume() {
        super.onResume();
        HTTPClientHelper.getCodeByIdAndUpdateView(this,codeEntity);
    }

    public TextView getCodeContentTextView() {
        return codeContentTextView;
    }

    public void setCodeContentTextView(TextView codeContentTextView) {
        this.codeContentTextView = codeContentTextView;
    }
}
