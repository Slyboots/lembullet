package pl.kn_pirm.pwr.lembullet.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by naraj on 02.07.16.
 */
public class Calling {

    private Integer id;
    private String title, description;
    private CallingStatus status;
    private Float latitude, longitude;

    public Calling() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CallingStatus getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = CallingStatus.values()[status];
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getLatitudeToString() {
        return latitude == null ? "" : String.valueOf(latitude);
    }

    public String getLongitudeToString() {
        return longitude == null ? "" : String.valueOf(longitude);
    }

    public String getStatusToString() {
        return status.name();
    }

    public static Calling fromJSON(JSONObject obj) {
        Calling calling = null;
        try {
            calling = new Calling();
            calling.setId(obj.getString("id").equals("null") ? null : Integer.valueOf(obj.getString("id")));
            calling.setTitle(obj.getString("title").equals("null") ? "" : obj.getString("title"));
            calling.setDescription(obj.getString("description").equals("null") ? "" : obj.getString("description"));
            calling.setStatus(obj.getString("status").equals("null") ? 0 : Integer.valueOf(obj.getString("status")));
            calling.setLatitude(obj.getString("latitude").equals("null") ? null : Float.valueOf(obj.getString("latitude")));
            calling.setLongitude(obj.getString("longitude").equals("null") ? null : Float.valueOf(obj.getString("longitude")));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calling;
    }

    public static ArrayList<Calling> fromJSON(JSONArray jsonArray) {
        ArrayList<Calling> callingsList = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject calling;
                calling = jsonArray.getJSONObject(i);
                callingsList.add(Calling.fromJSON(calling));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return callingsList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public enum CallingStatus {
        PENDING,
        ACCEPTED,
        DECLINED,
        COMPLETED
    }
}
