package pl.kn_pirm.pwr.lembullet.events;

import java.util.ArrayList;

import pl.kn_pirm.pwr.lembullet.model.Wanted;

/**
 * Created by naraj on 05.07.16.
 */
public class WantedUpdateEvent {

    public ArrayList<Wanted> wanted;

    public WantedUpdateEvent(ArrayList<Wanted> wanted) {
        this.wanted = wanted;
    }
}
