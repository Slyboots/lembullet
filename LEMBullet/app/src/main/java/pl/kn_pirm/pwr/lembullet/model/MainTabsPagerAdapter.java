package pl.kn_pirm.pwr.lembullet.model;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import pl.kn_pirm.pwr.lembullet.R;
import pl.kn_pirm.pwr.lembullet.view.DiagnosticFragment;
import pl.kn_pirm.pwr.lembullet.view.MapFragment;
import pl.kn_pirm.pwr.lembullet.view.CallingFragment;

/**
 * Created by slyboots on 01.05.16.
 */
public class MainTabsPagerAdapter extends FragmentStatePagerAdapter {

    Context context;

    public MainTabsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment frag = null;
        switch (position) {
            case 0:
                frag = new MapFragment();
                break;
            case 1:
                frag = new DiagnosticFragment();
                break;
            case 2:
                frag = new CallingFragment();
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = " ";
        switch (position) {
            case 0:
                title = context.getResources().getString(R.string.mapTab);
                break;
            case 1:
                title = context.getResources().getString(R.string.diagnosticTab);
                break;
            case 2:
                title = context.getResources().getString(R.string.requestTab);
                break;
        }

        return title;
    }

}