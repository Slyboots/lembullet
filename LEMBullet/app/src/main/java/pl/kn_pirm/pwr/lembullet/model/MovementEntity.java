package pl.kn_pirm.pwr.lembullet.model;

import android.location.Location;

import java.io.Serializable;

/**
 * Created by slyboots on 29.06.16.
 */
public class MovementEntity implements Serializable{


    private double speed;
    private double latitude;
    private double longitude;
    private long timeStamp;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    static final long serialVersionUID = 4321412;
    public MovementEntity(){
    }


    public MovementEntity(double speed, double latitude, double longitude) {
        this.speed = speed;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {

        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
